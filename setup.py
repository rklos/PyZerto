from setuptools import setup

setup(
    install_requires=[
        'requests'
    ],
    name='pyZerto',
    version='1.86',
    packages=['pyzerto']
)
