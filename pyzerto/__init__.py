# -*- coding: utf-8 -*-
'''
Module with api interface for zerto
'''
import base64
import json
import requests
import logging

from pyzerto.errors import (                        # NOQA
    ZertoError,
    ZertoUnsupportedApi,
    ZertoServiceError,
    ZertoAuthError,
    Zerto4xx,
    ZertoBadRequest,
    ZertoUnauthorized,
    ZertoForbidden,
    ZertoNotFound,
    ZertoMethodNotAllowed,
    ZertoFailure,
)
from pyzerto.constants import (                     # NOQA
    ZertoConstant,
    ZertoConstantDict,
    AuthenticationMethod,
    authentication_method,
    CommitPolicy,
    commit_policy,
    EntityType,
    entity_type,
    EventCategory,
    event_category,
    EventType,
    event_type,
    PairingStatus,
    pairing_status,
    SiteType,
    site_type,
    TaskState,
    task_state,
    VMType,
    vm_type,
    VPGPriority,
    vpg_priority,
    VPGStatus,
    vpg_status,
    VPGSubStatus,
    vpg_sub_status,
    VRAStatus,
    vra_status,
)
from pyzerto.zertoobject import ZertoObject         # NOQA
from pyzerto.alert import Alert                     # NOQA
from pyzerto.localsite import LocalSite             # NOQA
from pyzerto.peersite import PeerSite               # NOQA
from pyzerto.event import Event                     # NOQA
from pyzerto.task import Task                       # NOQA
from pyzerto.vm import VM                           # NOQA
from pyzerto.vpg import VPG                         # NOQA
from pyzerto.vra import VRA                         # NOQA
from pyzerto.zorg import ZORG                       # NOQA
from pyzerto.checkpoint import CHECKPOINT, CHECKPOINTSTATUS  # NOQA
from pyzerto.serviceprofile import ServiceProfile   # NOQA
from pyzerto.virtualization_site import VirtualizationSite  # NOQA
from pyzerto.unprotected_vm import UnprotectedVm  # NOQA

#Should Check for Valid Session and revalidate if required

try:  # Python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass

log = logging.getLogger(__name__)
log.addHandler(NullHandler())
log.setLevel(logging.DEBUG)

class Zerto(object):

    def __init__(self, url, session=None):
        self.url = url
        self.session = session
        self.paths = ['v1','ZvmService']

    def get_url(self, path):
        if not self.url:
            raise ValueError('Invalid url')
        base_path = '/'.join(path.strip('/').split('/')[:2])
        #print(base_path)
        #if base_path not in self.paths:
        #    raise ZertoUnsupportedApi(path)
        url = '{0}/{1}'.format(self.url.rstrip('/'), path.lstrip('/'))
        return url

    def _do_request(self, method, path, data=None, headers=None, **kwargs):
        url = self.get_url(path)
        log.debug('Executing {} request to {}'.format(method,url))
        kwargs = dict([(k, v) for k, v in kwargs.items() if v is not None])
        if data is not None:
            kwargs['data'] = data
        if headers is None:
            headers = {
                'content-type': 'application/json',
                'Accept': 'application/json',
                }
        #Zerto wont create a new session if the headers contain a sessionid
        if self.session and path != 'v1/session/add':
            headers.update({
                'x-zerto-session': self.session,
            })

        if headers:
            kwargs['headers'] = headers
        if 'verify' not in kwargs:
            kwargs['verify'] = False

        req = getattr(requests, method.lower())(url, **kwargs)
        if req.status_code == 200:
            return req
        try:
            result = req.json()
        except:
            result = {}
        if isinstance(result, dict):
            req.errcode = result.get('errorCode')
            req.errmsg = result.get('errorMessage')
            log.error('Status Code: {}'.format(req.status_code))
            log.error('Status Body: {}'.format(req.text))
            log.error('Error Code: {}'.format(req.errcode))
            log.error('Error Message: {}'.format(req.errmsg))
        else:
            req.errcode = None
            req.errmsg = '{0}'.format(result)
        params = kwargs.get('params')
        if 400 <= req.status_code < 500:
            if req.status_code == 400:
                errcls = ZertoBadRequest
            elif req.status_code == 401:
                errcls = ZertoUnauthorized
            elif req.status_code == 403:
                errcls = ZertoForbidden
            elif req.status_code == 404:
                errcls = ZertoNotFound
            elif req.status_code == 405:
                errcls = ZertoMethodNotAllowed
            else:
                errcls = Zerto4xx
            raise errcls(
                req.status_code, req.errcode, req.errmsg,
                method.upper(), path, params, data)
        if 500 <= req.status_code < 600:
            raise ZertoFailure(
                req.status_code, req.errcode, req.errmsg,
                method.upper(), path, params, data)
        raise ZertoServiceError(
            req.status_code, req.errcode, req.errmsg,
            method.upper(), path, params, data)

    def get_request(self, path, **kwargs):
        return self._do_request('GET', path, **kwargs)

    def post_request(self, path, data=None, **kwargs):
        return self._do_request('POST', path, data, **kwargs)

    def put_request(self, path, data=None, **kwargs):
        return self._do_request('PUT', path, data, **kwargs)

    def delete_request(self, path, **kwargs):
        return self._do_request('DELETE', path, **kwargs)

    def get_apis(self):
        headers = {'content-type': 'application/json'}
        req = self.get_request('v1', headers=headers)
        self.paths = list(sorted(['v1'] + [
            i['href'].split('/', 3)[-1].strip('/')
            for i in req.json()
        ]))
        return req.json()

    def get_session(self, user=None, password=None, method=None, creds=None):
        if not self.paths:
            self.get_apis()
        if creds:
            log.debug('Creds Preset: {}'.format(creds))
            headers = {
            'Authorization': (b'Basic ') + creds
        }
        else:    
            headers = {
                'Authorization': (b'Basic ') + base64.b64encode(bytes('{0}:{1}'.format(user, password), "utf-8"))
            }
        session = None
        path = 'v1/session/add'
        if method is not None and not isinstance(method, AuthenticationMethod):
            try:
                method = authentication_method[method]
            except KeyError:
                raise ZertoAuthError(
                    'Invalid authentication method {0}'.format(method))
        if method is None or method.code == 0:
            # Default is windows authentication
            try:
                req = self.post_request(path, headers=headers)
                if req.status_code == requests.codes.ok:
                    session = req.headers.get('x-zerto-session')
            except ZertoUnauthorized:
                pass
        if not session and (method is None or method.code == 1):
            # Try or retry AuthenticationMethod 1 (VirtualizationManager)
            log.debug('Logging in with vm auth')
            headers['content-type'] = 'application/json'
            try:
                req = self.post_request(
                    path,
                    json.dumps({'AuthenticationMethod': 1}),
                    headers=headers,
                )
                if req.status_code == requests.codes.ok:
                    session = req.headers.get('x-zerto-session')
            except ZertoUnauthorized:
                pass
            except Exception as e:
                raise Exception(e)
        if not session:
            print('Failed to get session')
            raise ZertoAuthError('Invalid user name and/or password')
        self.session = session
    
    def del_session(self):
        path = 'v1/session/delete'
        req = self.delete_request(path)


    def get_localsite(self, status=None):
        if status:
            req = self.get_request('v1/localsite/pairingstatuses')
            return req.json()
        req = self.get_request('v1/localsite')
        return LocalSite(**req.json())

    def get_peersites(self, siteid=None, status=None, **kwargs):
        if status:
            req = self.get_request('v1/peersites/pairingstatuses')
            return req.json()
        elif siteid is not None:
            req = self.get_request('v1/peersites/{0}'.format(siteid))
            return PeerSite(**req.json())
        req = self.get_request('v1/peersites', params=(kwargs or None))
        return list([PeerSite(**res) for res in req.json()])

    def get_alert(self, alert=None):
        if alert is not None:
            req = self.get_request('v1/alerts/{0}'.format(alert))
            return Alert(**req.json())
        req = self.get_request('v1/alerts')
        return list([Alert(**res) for res in req.json()])

    def get_event(self, event=None, **kwargs):
        '''Retrieve specific event or all'''
        if event is not None:
            req = self.get_request('v1/events/{0}'.format(event))
            return Event(**req.json())
        req = self.get_request('v1/events', params=(kwargs or None))
        return list([Event(**res) for res in req.json()])

    def get_event_categories(self):
        req = self.get_request('v1/events/categories')
        return req.json()

    def get_event_entities(self):
        req = self.get_request('v1/events/entities')
        return req.json()

    def get_event_types(self):
        req = self.get_request('v1/events/types')
        return req.json()

    def get_serviceprofiles(self, serviceprofile=None, **kwargs):
        if serviceprofile is not None:
            req = self.get_request(
                'v1/serviceprofiles/{0}'.format(serviceprofile))
            return ServiceProfile(**req.json())
        req = self.get_request(
            'v1/serviceprofiles', params=(kwargs or None))
        return list([ServiceProfile(**res) for res in req.json()])

    def get_task(self, task=None, **kwargs):
        if task is not None:
            req = self.get_request('v1/tasks/{0}'.format(task))
            return Task(**req.json())
        req = self.get_request('v1/tasks', params=(kwargs or None))
        return list([Task(**res) for res in req.json()])

    def get_virtualization_site(self, siteid=None):
        if siteid is not None:
            req = self.get_request(
                'v1/virtualizationsites/{0}'.format(siteid))
            return VirtualizationSite(**req.json())
        req = self.get_request('v1/virtualizationsites')
        return list([VirtualizationSite(**res) for res in req.json()])
        
    def get_virtualization_site_rp(self, siteid=None):
        if siteid is not None:
            req = self.get_request(
                'v1/virtualizationsites/{0}/resourcepools'.format(siteid))
            return req.json()
        req = self.get_request('v1/virtualizationsites')
        return list([VirtualizationSite(**res) for res in req.json()])

    def get_unprotected_vms(self, siteid=None):
        if siteid is not None:
            req = self.get_request(
                'v1/virtualizationsites/{0}/vms'.format(siteid))
            #return list([UnprotectedVm(**res) for res in req.json()])
            return req.json()

    def get_vpg_settings(self, vpg_set_id=None):
        if vpg_set_id is not None:
            req = self.get_request(
                'v1/vpgSettings/{}'.format(vpg_set_id))
            return req.json()

        req = self.get_request(
                'v1/vpgSettings')
        return req.json()

    def create_vpg_settings(self, vpgid=None):
        '''Create VPG Setting Object'''
        if vpgid is not None:
            data={"VpgIdentifier":vpgid}
            req = self.post_request(
                'v1/vpgSettings',data=json.dumps(data))
            return req.json()
        req = self.post_request(
                'v1/vpgSettings')
        return req.json()
        
    def delete_vpg_settings(self, vpg_set_id):
        '''Delete VPG Setting Object'''
        req = self.delete_request(
              'v1/vpgSettings/{}'.format(vpg_set_id))
        return

    def get_vm(self, vmid=None, **kwargs):
        '''Retrieve specific vm or all'''
        if vmid is not None:
            req = self.get_request('v1/vms/{0}'.format(vmid))
            return VM(**req.json())
        req = self.get_request('v1/vms', params=(kwargs or None))
        return list([VM(**res) for res in req.json()])

    def get_vpg(self, vpgid=None, **kwargs):
        '''Retrieve specific vpg or all'''
        if vpgid is not None:
            req = self.get_request('v1/vpgs/{0}'.format(vpgid))
            return VPG(**req.json())
        req = self.get_request('v1/vpgs', params=(kwargs or None))
        return list([VPG(**res) for res in req.json()])
        
    def get_vpg_checkpoints(self, vpgid, **kwargs):
        '''Retrieve specific vpg checkpoints'''
        req = self.get_request('v1/vpgs/{0}/checkpoints'.format(vpgid))
        return list([CHECKPOINT(**res) for res in req.json()])
        
    def get_vpg_checkpoint_stats(self, vpgid):
        '''Retrieve specific vpg checkpoint status'''
        req = self.get_request('v1/vpgs/{0}/checkpoints/stats'.format(vpgid))
        return list([CHECKPOINTSTATUS(**res) for res in req.json()])

    def get_vra(self, vraid=None, **kwargs):
        if vraid is not None:
            req = self.get_request('v1/vras/{0}'.format(vraid))
            return VRA(**req.json())
        req = self.get_request('v1/vras', params=(kwargs or None))
        return list([VRA(**res) for res in req.json()])

    def get_zorg(self, zorgid=None):
        if zorgid is not None:
            req = self.get_request('v1/zorgs/{0}'.format(zorgid))
            return ZORG(**req.json())
        req = self.get_request('v1/zorgs')
        return list([ZORG(**res) for res in req.json()])

    def get_resources_report(self, **kwargs):
        # fromTimeString={fromTimeString}
        # toTimeString={toTimeString}
        # startIndex={startIndex}
        # count={count}
        # filter={filter}
        if 'filter' in kwargs:
            req = self.get_request(
                'ZvmService/ResourcesReport/getSamplesWithFilter',
                params=(kwargs or None),
            )
        else:
            req = self.get_request(
                'ZvmService/ResourcesReport/getSamples',
                params=(kwargs or None),
            )
        return req.json()
        
    def start_vpg_test_failover(self, vpgid, checkpoint=None):
        data=""
        if checkpoint is not None:
            data={"VpgIdentifier":vpgid}
        req = self.post_request(
                '/v1/vpgs/{}/FailoverTest'.format(vpgid),data=data)
        return req.json()
        
    def stop_vpg_test_failover(self, vpgid, testsuccess=True,testsummary=""):
        data={"FailoverTestSuccess":testsuccess ,"FailoverTestSummary":testsummary}
        req = self.post_request(
                '/v1/vpgs/{}/FailoverTestStop'.format(vpgid),data=json.dumps(data))
        return req.json()

    def start_vpg_live_failover(self, **kwargs):
        data={}
        if 'checkpoint' in kwargs:
            data["CheckpointIdentifier"]=kwargs.get('checkpoint')
        data["CommitPolicy"]=kwargs.get('commit_policy')
        data["CommitValue"]=kwargs.get('commit_value')
        data["ShutdownPolicy"]=kwargs.get('shutdown_policy')
        data["TimeToWaitBeforeShutdownInSec"]=kwargs.get('shutdown_value')
        if 'reverse' in kwargs:
            data["IsReverseProtection"]=kwargs.get('reverse')
        
        req = self.post_request(
                '/v1/vpgs/{}/Failover'.format(kwargs.get('id')),data=json.dumps(data))
        return req.json()

    def commit_vpg_live_failover(self, vpgid, reverse=False):
        data={"IsReverseProtection":reverse}
        req = self.post_request(
                '/v1/vpgs/{}/FailoverCommit'.format(vpgid), data=json.dumps(data))
        return req.json()

    def rollback_vpg_live_failover(self, vpgid):
        req = self.post_request(
                '/v1/vpgs/{}/FailoverRollback'.format(vpgid))
        return req.json()

    def syncronize_vpg(self, vpgid):
        req = self.post_request(
                '/v1/vpgs/{}/forcesync'.format(vpgid))
        return req.json()

    def pause_vpg(self, vpgid):
        req = self.post_request(
                '/v1/vpgs/{}/pause'.format(vpgid))
        return req.json()

    def resume_vpg(self, vpgid):
        req = self.post_request(
                '/v1/vpgs/{}/resume'.format(vpgid))
        return req.json()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
