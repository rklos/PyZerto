# -*- coding: utf-8 -*-
'''Zerto VPG object'''

from pyzerto.zertoobject import ZertoObject
from pyzerto.misc import parse_timestamp


class CHECKPOINT(ZertoObject):

    def __init__(self, **kwargs):
        kwargs['TimeStamp'] = parse_timestamp(kwargs.get('TimeStamp'))
        self.values = kwargs
        self.tag = kwargs['Tag']
        self.identifier = kwargs['CheckpointIdentifier']
        self.priority = kwargs.get('TimeStamp')
        

    def __str__(self):
        return 'identifier={}'.format(self.identifier)
        
class CHECKPOINTSTATUS(ZertoObject):

    def __init__(self, **kwargs):
        self.values = kwargs
        self.earliest = CHECKPOINT(**kwargs['Earliest'])
        self.latest = CHECKPOINT(**kwargs['Latest'])
        



